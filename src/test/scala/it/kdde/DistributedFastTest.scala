package it.kdde

//import it.kdde.distributed.fast.{DistributedDataset, DistributedFast}
import it.kdde.distributed.sequentialpatterns.{DFast, DistributedFast}
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.FlatSpec
import org.scalatest.BeforeAndAfterEach

import scala.util.Properties
/**
 * Created by fabiana on 2/3/15.
 */
class DistributedFastTest extends FlatSpec with BeforeAndAfterEach{

  implicit var sc: SparkContext = _
  val algorithmType = DFast.AssociationAlgorithm.fromString("frequent")
  val lines = 52467
  val inputFile =  getClass.getClassLoader.getResource("./bpi2015/dataset.txt").getPath

  override def beforeEach() {
    val master = Properties.envOrElse("MASTER", "local")
    val conf = new SparkConf().setAppName("PmmWithFastSask")
    sc = new SparkContext(master, "PmmWithFast_test", conf)

    super.beforeEach() // To be stackable, must call super.beforeEach
  }

  behavior of "A distributed Fast algorithm"

  it should "should generate a right number of frequent sequences on bp12015 dataset" in {
    println("************************************************************++")
    val dataset = sc.textFile(inputFile, 1)
    println("************************************************************++")
    val patterns = DFast.train(dataset, algorithmType, 0.80f, 1f).collect()
    println("************************************************************++")

   // val distributedDataset = DistributedDataset(dataset, 0)
   //val distributedFast = DistributedFast(distributedDataset)
   //val res = distributedFast.getFrequentSequences()
    // assert(res.length === 142)

    //patterns.foreach(x => println(x._1))

    println(s"Generated ${patterns.size} frequent sequences")
  }

}
