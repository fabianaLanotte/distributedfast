package it.dtk.stackowerflow

import it.dtk.stackoverflow.Model._
import it.dtk.stackoverflow.hbase.HbaseConverter._
import it.dtk.stackoverflow.hbase.ModelDAO._
import it.nerdammer.spark.hbase._
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by fabiofumarola on 18/02/15.
 */
object SparkGetUserTest extends App {

  val sparkConf = new SparkConf()
  sparkConf.set("spark.hbase.host", "localhost")
  implicit val sc = new SparkContext("local", "Main", sparkConf)

  val user = UserDAO.findUser("200")
  println(user)
}
