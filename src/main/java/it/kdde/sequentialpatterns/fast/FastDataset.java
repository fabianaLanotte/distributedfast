package it.kdde.sequentialpatterns.fast;


import it.kdde.sequentialpatterns.model.SparseIdList;
import it.kdde.util.Utils;
import scala.Serializable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 * this containsItemset the representation of a dataset for Fast
 */
public class FastDataset implements Serializable {

    public static final String ITEMSET_SEPARATOR = "-1";
    public static final String ITEM_DATA_SEPARATOR = "##";
    public static final String ITEM_SEPARATOR = " :: ";
    public static final String SEQUENCE_SEPARATOR = "-2";

    /**
     * Associates to each frequent itemset its SparseIdList
     */
    private Map<String, SparseIdList> itemSILMap;
    private final long numRows;
    private final float minSup;
    private int absMinSup;


    /**
     * @param numRows
     * @param minSup
     */
    private FastDataset(long numRows, float minSup) {
        this.itemSILMap = new HashMap<String, SparseIdList>();
        this.numRows = numRows;
        this.minSup = minSup;
        absMinSup = Utils.absoluteSupport(minSup, numRows);
        if (absMinSup == 0)
            absMinSup = 1;
    }

    public FastDataset(Map<String, SparseIdList> itemsSILMap, long numRows, float minSup, int absMinSup) {
        this.itemSILMap = itemsSILMap;
        this.numRows = numRows;
        this.minSup = minSup;
        this.absMinSup = absMinSup;
    }

    /**
     * Finds all frequent 1 items
     */
    private void computeFrequentItems() {
        final Map<String, SparseIdList> newMap = new TreeMap<String, SparseIdList>();

        for (Map.Entry<String, SparseIdList> pair : itemSILMap.entrySet()) {
            if (pair.getValue().getAbsoluteSupport() >= absMinSup)
                newMap.put(pair.getKey(), pair.getValue());
        }

        itemSILMap = newMap;
    }

    public Map<String, SparseIdList> getFrequentItemsets() {
        return itemSILMap;
    }

    /**
     * Get the SparseIdList for a particular item
     *
     * @param item
     * @return a SparseIdList, return null if that SparseIdList doesn't exist in
     * dataset
     */
    public SparseIdList getSparseIdList(String item) {
        return itemSILMap.get(item);
    }


    public long getNumRows() {
        return numRows;
    }

    /**
     * "to change with abs count"
     *
     * @return
     */
    @Deprecated
    public float getMinSup() {
        return minSup;
    }

    public int getAbsMinSup() {
        return absMinSup;
    }

    private static long countLines(String path) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(path));
        long count = 0;
        while (in.readLine() != null)
            count++;

        in.close();
        return count;
    }

    /**
     * @param path
     * @param relativeSupport
     * @return
     */
    public static FastDataset fromPrefixspanSource(String path, float relativeSupport) throws IOException {

        long numRows = countLines(path);
        final FastDataset fastDataset = new FastDataset(numRows, relativeSupport);

        int lineNumber = 0;
        String line;
        BufferedReader in = new BufferedReader(new FileReader(path));
        while ((line = in.readLine()) != null) {

            if (line.length() == 0)
                continue;

            int transID = 1;

            StringTokenizer tokenizer = new StringTokenizer(line, ITEM_SEPARATOR);
            String token;
            while (tokenizer.hasMoreElements()) {
                token = tokenizer.nextToken();

                if (token.equals(ITEMSET_SEPARATOR)) {
                    transID++;
                    continue;
                }

                if (token.equals(SEQUENCE_SEPARATOR))
                    break;

                //support both data and not data element
                if (token.contains(ITEM_DATA_SEPARATOR)) {
                    String[] itemData = token.split(ITEM_DATA_SEPARATOR);
                    String item = itemData[0];
                    String data = itemData[1];
                    if (!fastDataset.itemSILMap.containsKey(item))
                        fastDataset.itemSILMap.put(item, new SparseIdList((int) numRows));

                    fastDataset.itemSILMap.get(item).addElement(lineNumber, transID, data);
                } else {
                    if (!fastDataset.itemSILMap.containsKey(token))
                        fastDataset.itemSILMap.put(token, new SparseIdList((int) numRows));
                    fastDataset.itemSILMap.get(token).addElement(lineNumber, transID, null);
                }

            }
            lineNumber++;
        }
        fastDataset.computeFrequentItems();
        return fastDataset;
    }

//    /**
//     * @param path
//     * @return
//     * @throws java.io.IOException
//     */
//    private static long countNumRowsSpamSource(Path path) throws IOException {
//        Set<String> custIds = Files.lines(path).
//                filter(l -> l.length() > 0).
//                map(l -> l.split(" ")[0]).collect(Collectors.toSet());
//
//        return custIds.size();
//
//    }

//    /**
//     *
//     * @param path
//     * @param relativeSupport
//     * @return
//     * @throws java.io.IOException
//     */
//    public static FastDataset fromSpamSource(Path path, float relativeSupport) throws IOException {
//
//        long numRows = countNumRowsSpamSource(path);
//        final FastDataset fastDataset = new FastDataset(numRows, relativeSupport);
//
//        Files.lines(path).filter(l -> l.length() > 0).forEach(l -> {
//            String[] split = l.split(" ");
//            int custId = Integer.parseInt(split[0]);
//            int transId = Integer.parseInt(split[1]);
//
//            SparseIdList inserted = fastDataset.itemSILMap.putIfAbsent(split[2], new SparseIdList((int) numRows));
//            inserted.addElement(custId,transId, null);
//        });
//        fastDataset.computeFrequentItems();
//        return fastDataset;
//    }
}
