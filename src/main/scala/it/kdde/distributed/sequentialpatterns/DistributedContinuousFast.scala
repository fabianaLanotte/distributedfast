package it.kdde.distributed.sequentialpatterns

import it.kdde.sequentialpatterns.fast.{ Fast, FastDataset }
import it.kdde.sequentialpatterns.model.tree.SequenceNode
import it.kdde.sequentialpatterns.model.{ ListNode, SparseIdList, VerticalIdList }

import scala.collection.JavaConverters._

/**
 * Created by fabiana on 2/9/15.
 */

object DistributedContinuousFast {

  def apply(dataset: Dataset): DistributedContinuousFast = {
    val javaMap = dataset.silsMap.foldLeft(new java.util.TreeMap[String, SparseIdList]()) {
      case (acc, value) =>
        acc.put(value._1, value._2)
        acc
    }
    val datasetFast = new FastDataset(javaMap, dataset.numRows, dataset.minSupp, dataset.absoluteMinSupp)
    val fast = new Fast(datasetFast)
    fast.run()
    val ds = new DistributedContinuousFast(dataset, fast)
    ds.generateContiguousSequences(fast.getTree.getRoot)
    ds

  }

  def apply(fastDataset: Dataset, fast: Fast): DistributedContinuousFast = {
    val ds = new DistributedContinuousFast(fastDataset, fast)
    ds.generateContiguousSequences(fast.getTree.getRoot)
    ds
  }

}

class DistributedContinuousFast(val dataset: Dataset, val fast: Fast) {

  var contiguousSequences: List[(String, Seq[Seq[ListNode]])] = List()
  var openingSequences: List[(String, Seq[Seq[ListNode]])] = List()

  /**
   * For contiguous sequences it is need to know the first occurrence where the sequence is contiguous
   *
   * @param sequence
   * @return
   */
  private def mapSequenceForClassification(sequence: SequenceNode): (String, Seq[Seq[ListNode]]) = {

    var list = List[VerticalIdList]()
    var node = sequence

    val lastNodeElements = sequence.getVerticalIdList.getElements

    val rows = for (i <- 0 until lastNodeElements.size if (lastNodeElements(i) != null))
      yield Seq(lastNodeElements(i))

    (sequence.getSequence.toString, rows)
  }

  /**
   * If node child is contiguous to its parent then returns the VerticalIdList of the first occurrence, otherwise None
   *
   * @param node
   * @return
   */
  private def getContiguousVil(node: SequenceNode): Option[Seq[Seq[ListNode]]] = {

    //val support = this.dataset.absoluteMinSupp
    var contiguousSupp = 0
    var parents: List[SequenceNode] = List(node)
    var p = node.getParent
    while (p != this.fast.getTree.getRoot) {
      parents = parents :+ p
      p = p.getParent
    }

    var res: Seq[Seq[ListNode]] = Seq()
    var nodeVil = node.getVerticalIdList.getElements
    // var newVil: VerticalIdList =  new VerticalIdList(new Array[ListNode](node.getVerticalIdList.getElements.length),0)
    for (i <- 0 until nodeVil.size) {
      var list: Seq[ListNode] = Seq(nodeVil(i))
      //if true update the the i-th element of node's Vil
      var isShifted = false
      Option(nodeVil(i)) match {
        case Some(_) =>
          var p = 1
          var isContiguousI = true
          while (p < parents.size) {
            val columnNode = nodeVil(i).getColumn
            var columnParent = parents(p).getVerticalIdList.getElements()(i)
            while ((columnParent != null) && (columnParent.getColumn < (columnNode - p))) {
              columnParent = columnParent.next()
            }

            if (columnParent == null || columnParent.getColumn != (columnNode - p)) {
              nodeVil(i) = nodeVil(i).next()
              isShifted = true
              p = 1
              list = Seq(nodeVil(i))
            }
            else {
              p += 1
              list = columnParent +: list

            }
            if (nodeVil(i) == null) {
              isContiguousI = false
              p = parents.size
            }

          }
          if (isContiguousI) {
            res = res :+ list
            contiguousSupp += 1

          }
        case None => getOpeningSequences()

      }

    }

    contiguousSupp >= this.dataset.absoluteMinSupp match {
      case true  => Some(res)
      case false => None
    }

  }

  private def isOpeningSequence(node: SequenceNode, vil: VerticalIdList): Boolean = {
    val count = vil.getElements.count(i =>
      i != null && i.getColumn == node.getSequence.getElements.size())

    count >= dataset.absoluteMinSupp
  }
  private def isOpeningSequence(node: SequenceNode, rows: Seq[Seq[ListNode]]): Boolean = {
    val count = rows.count { sequence =>
      val lastListNode = sequence(sequence.size - 1)
      lastListNode != null && lastListNode.getColumn == node.getSequence.getElements.size()
    }

    count >= dataset.absoluteMinSupp
  }

  /**
   * Give a node check if it is contiguous and/or opening and if true add it to contiguousSequences/openingSequences lists
   * @param node
   */
  private def generateContiguousSequences(node: SequenceNode): Unit = {

    val children = node.getChildren.asScala.toList
    children.foreach { child =>
      if (child.getParent == this.fast.getTree.getRoot) {
        val convertedSequence = mapSequenceForClassification(child)
        this.contiguousSequences = this.contiguousSequences :+ ((convertedSequence._1), convertedSequence._2)

        if (isOpeningSequence(child, child.getVerticalIdList)) {
          this.openingSequences = this.openingSequences :+ (convertedSequence._1, convertedSequence._2)
        }
        generateContiguousSequences(child)
      }
      else {
        getContiguousVil(child) match {
          case Some(rows) =>
            //val convertedSequence = mapSequenceForClassification(child, child.getVerticalIdList)

            contiguousSequences = contiguousSequences :+ (child.getSequence.toString, rows)

            if (isOpeningSequence(child, rows))
              openingSequences = openingSequences :+ (child.getSequence.toString, rows)
            generateContiguousSequences(child)
          case None =>
        }
      }
    }
  }

  def getContiguousSequences(): List[(String, Seq[Seq[ListNode]])] = this.contiguousSequences
  def getOpeningSequences(): List[(String, Seq[Seq[ListNode]])] = this.openingSequences

}
