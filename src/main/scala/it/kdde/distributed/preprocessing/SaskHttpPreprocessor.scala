package it.kdde.distributed.preprocessing

import java.text.SimpleDateFormat
import java.util.Locale

import it.kdde.distributed.sequentialpatterns.Dataset
import it.kdde.sequentialpatterns.model.ListNode
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.joda.time.format.DateTimeFormat
import org.json4s._
import org.json4s.jackson.JsonMethods._

/**
 * Created by fabiofumarola on 24/03/15.
 */
object SaskHttpPreprocessor extends Serializable with Preprocessor {

  case class Connection(timestamp: Long,
                        requestType: String, // GET POST, PUT, DELETE
                        replyCode: Int, //200, 400, 304, 500
                        bytesReply: Long,
                        regressionValue: Option[Double] = None,
                        classificationValue: Option[Double] = None)

  override def categoricalFeatures: Map[Int, Int] = Map(1 -> 4)
  override def numberFeatures = 4

  val requestCodeMap = Map("GET" -> 0, "POST" -> 1, "PUT" -> 2, "DELETE" -> 3)

  val featuresToIgnore = List(4, 5)


  //SID = host   host: String, //www.google.it, www.wheretolive.it
  //TID = resource   resource: String, // /path/~/file

  case class SequenceElem(
    sid: String,
    tid: String,
    data: Connection)

  val pattern = """(.+)\s-\s-\s\[(.+)\]\s"(GET|POST|PUT|DELETE)\s(.+)"\s(\d+)\s(\d+)""".r

  private def processLines(lines: RDD[String]): RDD[SequenceElem] = {
    lines
      .map(processLine)
      .filter(_.isDefined)
      //.filter(e => e.get.tid.endsWith("/") || e.get.tid.endsWith("html"))
      .map(_.get)
  }

  private def processLine(line: String): Option[SequenceElem] = {
    val dateFormatter = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss Z", Locale.ENGLISH) //DateTimeFormat.forPattern("dd/MMM/YYYY:HH:mm:ss Z")
    line match {
      case pattern(host, timestamp,requestType, resource, replyCode, bytesReply) =>
        Some(SequenceElem(
          sid = host,
          tid = resource,
          data = Connection(
            timestamp = dateFormatter.parse(timestamp).getTime,
            requestType = requestType,
            replyCode = replyCode.toInt,
            bytesReply = bytesReply.toLong
          )
        ))

      case _ =>
        None
    }
  }

  /**
   * *
   *
   * @param lines
   * @return a row in the the rdd is composed by
   *         tid :: jsonSerialized -1 tid :: jsonSerialized -1 -2
   */
  override def generateSequenceDataset(lines: RDD[String], minLength: Int)(implicit sc: SparkContext): RDD[String] = {

    val elements = processLines(lines)

    val tids = elements.groupBy(_.tid).map(kv => kv._1).collect()
    val index = for(i <- 1 to tids.size) yield i
    val mapTidIndex = tids.zip(index).toMap

    val elementsGrouped = elements.groupBy(_.sid)

    val sequences = elementsGrouped.
      filter(_._2.toList.size >= minLength).
      map {
        case (sid, elements) =>
          val sorted = elements.toList.sortWith(_.data.timestamp < _.data.timestamp)
          val lastTime = sorted.last.data.timestamp
          //slide the elements to have the current and the next activity
          val slided = sorted.sliding(2)
          val sequence = slided.foldLeft(""){
            case (acc, List(current, next)) =>
              val completionTime = lastTime-current.data.timestamp
              val newCurrentData = current.data.copy( requestType = requestCodeMap.get(current.data.requestType).get.toString, classificationValue = Some(mapTidIndex.get(next.tid).get), regressionValue = Some(completionTime))
              val newSequence = current.copy( tid = mapTidIndex.get(current.tid).get.toString, data = newCurrentData)
              acc + serialize(newSequence) + " -1 "
          }
          val lastItem = sorted.last
          val data = lastItem.data.copy( requestType = requestCodeMap.get(lastItem.data.requestType).get.toString, regressionValue = Some(0d), classificationValue = Some(0d))
          val lastSequence = lastItem.copy(tid = mapTidIndex.get(lastItem.tid).get.toString, data= data)
          val seqSerialized = serialize(lastSequence)

          val completeSequence = sequence + seqSerialized +  Dataset.ITEMSET_SEPARATOR + Dataset.SEQUENCE_SEPARATOR
          completeSequence


      }
    sequences
  }

  /**
   *
   * @param elem
   * @return the format is composed by
   *         tid :: jsonSerialized of data
   */
  def serialize(elem: SequenceElem): String = {
    implicit val formats = DefaultFormats
    val jValue = Extraction.decompose(elem.data)
    val dataAsString = compact(render(jValue))
    val itemSeparator = Dataset.ITEM_SEPARATOR
    s"${elem.tid}$itemSeparator$dataAsString"
  }

  /**
   * Vengono usati per trasformare ogni sequenza di  test in un Vettore di feature da dare in input al classificatore/regressore associato
   * @param row in the form tid1 :: {samedata} -1 tid2 :: {samedata} -1 -2
   * @return the vector of feature associate to sequence row
   */
  override def extractFeatureSequence(row: String): Vector = {
    implicit val formats = DefaultFormats
    val listTuples = row.split(" -1 ")

    //take the first n - 1 to remove the element with -2
    val array = listTuples.take(listTuples.length - 1).map { elem =>
      val pair = elem.split(" :: ")
      val connection = parse(pair(1)).extract[Connection]

      Array(
        connection.timestamp.toDouble,
        requestCodeMap.getOrElse(connection.requestType, 0).toDouble,
        connection.replyCode.toDouble,
        connection.bytesReply.toDouble
      )

    }.reduce(_ ++ _)

    Vectors.dense(array)
  }

  /**
   * Vengono usati nella classe Predictors per generare i regressori per ogni nodo dell'albero
   * @param row
   * @return
   */
  override def getLabeledPointForRegression(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    val rowLabel = parse(row.last.getData).extract[Connection]
    val label = rowLabel.regressionValue.get

    val rowVector = row.flatMap { listNode =>
      val data = parse(row.last.getData).extract[Connection]
      val r = Connection.unapply(data).get
      val indexedValues = r.productIterator.toList.zip(0 until r.productArity)

      val filtered = indexedValues.filterNot(kv => featuresToIgnore.contains(kv._2))
      filtered.map(_._1.toString.toDouble)

    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }

  /**
   * Vengono usati nella classe Predictors per generare i classificatori per ogni nodo dell'albero
   * @param row
   * @return
   */
  override def getLabeledPointForClassification(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    val rowLabel = parse(row.last.getData).extract[Connection]
    val label = rowLabel.classificationValue.get

    val rowVector = row.flatMap { listNode =>
      val data = parse(row.last.getData).extract[Connection]
      //val data = rawData.copy(requestType = requestCodeMap.get(rawData.requestType).get.toString)

      val r = Connection.unapply(data).get
      val indexedValues = r.productIterator.toList.zip(0 until r.productArity)

      val filtered = indexedValues.filterNot(kv => featuresToIgnore.contains(kv._2))
      val prova: List[Double] = filtered.map(_._1.toString.toDouble)
      prova

    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }

  /**
   *
   * @param json return the value of the labelClassification contained in the json
   */
  override def getLabelClassification(json: String): Double = {
    implicit val formats = DefaultFormats
    val sequence= parse(json).extract[Connection]
    sequence.classificationValue.get
  }

  override def getLabelRegression(json: String): Double = {
    implicit val formats = DefaultFormats
    val sequence= parse(json).extract[Connection]
    sequence.regressionValue.get
  }
}
